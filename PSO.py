'''
REF : Binary classification posed as a quadratically constrained quadratic
      programming and solved using particle swarm optimization
BY  : DEEPAK KUMAR and A G RAMAKRISHNAN
(Sa¯dhana¯, 2016)
'''
import numpy as np
from numpy import linalg as LA
import random


def PSO(X,x,vx,yfix,k,tmax,w,c1,c2,c3,c4,psi):
    xbi = x
    xbg = GlobalBestPosition(x,k,yfix)
    t=0
    while t<tmax:
        for i in range(k):
            t=t+1
            
            r1,r2,r3 = random.random(),random.random(),random.random()       
            idy = np.random.randint(len(X), size=1) 
            r4 = X[idy,:]
            vx[:,i] = w*vx[:,i] + c1*r1*(xbi[:,i]-x[:,i]) + c2*r2*(xbg-x[:,i]) + c3*r3*(yfix-x[:,i]) + c4*r4
            xprev = x[:,i]
            
            x[:,i] = x[:,i] + vx[:,i]
            
            if ellipoid(X,x[:,i]) > radiusE(X,psi):
                x[:,i] = xprev  
                #print("out of ellipoid")
                #print(ellipoid(X,x[:,i]),radiusE(X,psi))
                
            fxi = f(x[:,i],yfix) 
            fxbi = f(xbi[:,i],yfix)
            fxbg = f(xbg,yfix)
            
            if fxbi > fxi:
                xbi[:,i] = x[:,i]
            
            if fxbg > fxbi:
                xbg = xbi[:,i]
            
    return xbg

def splitting(Xdata,ydata):
    #แบ่งข้อมูลออกเป็นสองเซตตาม class
    X = Xdata[ydata[:]  > 0] # class 1
    Y = Xdata[ydata[:] == 0] # class 0
    return X,Y

def Initial(X,k):
    x = np.ones((k,len(X[0])))
    for u in range(k):
        x[u,:]=np.mean(X,axis=0) + random.random()
                
    vx = np.ones((k,len(X[0]))) # กำหนดความเร็วต้นของ Particle Swam เป็น 1
    x = np.transpose(x) # เตรียมข้อมูลให้เหมาะกับการคำนวณ
    vx = np.transpose(vx) # เตรียมข้อมูลให้เหมาะกับการคำนวณ
    return x,vx

def ConfidenceEllipsoid(X):
    E = np.cov(np.transpose(X)) # Estimate covariance matric ของ X
    lambda_, v = np.linalg.eig(E) # eigenvalue of EX
    lambda_ = np.sqrt(lambda_) # ความยาวแกนของ confidence ellipsoid ของ X
    return E,lambda_,v

def GlobalBestPosition(x,k,yfix):
    xbi = x
    xbg = x[:,0]
    for i in range(k):
        fbi = f(yfix,xbi[:,i])
        fbg = f(yfix,xbg)
        if fbg > fbi:
            xbg = xbi[:,i]
    
    return xbg 

def f(a,b):
    return dis(a,b)

def GETmean(X):
    mean = np.mean(X,axis=0)
    return mean

def ellipoid(X,x):
    E,lambda_,v=ConfidenceEllipsoid(X)
    E=np.linalg.inv(E)
#    L = np.eye(len(X[0]))
#    for i in range(len(X[0])):
#       L[i,i]=lambda_[i] 
#    
    xmean = GETmean(X)    
#    A = np.matmul(np.matmul(np.transpose(x-xmean),L),x-xmean)
    A = np.matmul(np.matmul(np.transpose(x-xmean),E),x-xmean)
    return A 

def radiusE(X,psi):
    E,lambda_,v=ConfidenceEllipsoid(X)
    rE = psi
    #for i in range(len(X[0])):
    #    rE = rE*lambda_[i]
        
    return rE

def dis(x,y):
    return  LA.norm(x-y,ord=2)

def predict(z,x,y):
    n = len(z)
    zp = np.empty(n)
    for i in range(n):
        if dis(z[i],x) < dis(z[i],y):
            zp[i]=1
        else:
            zp[i]=0
    
    return zp
        
def accpredict(zp,z):
    acc = np.empty(len(z))
    for i in range(len(z)):
        if zp[i]==z[i]:
            acc[i]=1
            
        else:
            acc[i]=0
            
    return np.mean(acc)