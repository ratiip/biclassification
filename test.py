import PSO
from sklearn import svm
from sklearn.datasets.samples_generator import make_blobs
from sklearn.preprocessing import StandardScaler
import matplotlib.pyplot as plt
from matplotlib.patches import Ellipse
import numpy as np
from sklearn.model_selection import KFold
from sklearn.metrics import accuracy_score
import time
from sklearn.neighbors import KNeighborsClassifier

#สร้างข้อมูล
Xdata, ydata = make_blobs(n_samples=1024, centers=2, 
                          n_features=2, random_state=112, 
                          cluster_std =7)

#normalization
Xdata = StandardScaler().fit_transform(Xdata)

#cross validation
print("----------------------------------------------------")    
n_folds = 16
acc = np.empty((n_folds))
Processingtime = np.empty((n_folds))
cv = KFold(n_splits=n_folds, shuffle=True, random_state=58310798)

print("boundary points nearest neighbors via PSO")
for i, (train, test) in enumerate(cv.split(Xdata)):
    s = time.time()
    #แบ่งข้อมูลเป็น 2 classes
    X,Y = PSO.splitting(Xdata[train],ydata[train])
    
    #กำหนดค่าคงที่สำหรับ PSO algorithm
    k = 10 # จำนวน Particle Swam
    tmax = 300 # จำนวนรอบที่ทำซ้ำ
    w  = 0.005 
    c1 = 0.005
    c2 = 0.005
    c3 = 0.025
    c4 = 0.02
    psi= 0.29
    
    #กำหนด Particle Swam และ its velocity
    x,vx = PSO.Initial(X,k)
    y,vy = PSO.Initial(Y,k)
    
    #กำหนด y in Y ที่ใกล้ ellipsoid X
    yfix = PSO.GETmean(Y)
    
    #เริ่ม algorithm
    xfix = PSO.PSO(X,x,vx,yfix,k,tmax,w,c1,c2,c3,c4,psi)
    yfix = PSO.PSO(Y,y,vy,xfix,k,tmax,w,c1,c2,c3,c4,psi)
    #print(xfix,yfix)
    
    z = Xdata[test]
    zp = PSO.predict(z,xfix,yfix)
    
    e = time.time()
    Processingtime[i]= e-s 
    acc[i]= PSO.accpredict(zp,ydata[test])
 
print("Accuracy = %.4f" % np.mean(acc))
print("CPUtime = %.4f sec" % np.mean(Processingtime))
print("----------------------------------------------------")


print("k nearest neighbors")  
acc = np.empty((n_folds))
Processingtime = np.empty((n_folds))
cv = KFold(n_splits=n_folds, shuffle=True, random_state=58310798)

for i, (train, test) in enumerate(cv.split(Xdata)):
    s = time.time()
    #model = svm.SVC()
    #weight=model.fit(Xdata[train],ydata[train])
    model = KNeighborsClassifier(n_neighbors = 7, n_jobs =2)
    model.fit(Xdata[train],ydata[train])
    y_p = model.predict(Xdata[test]) 
    acc[i]= accuracy_score(ydata[test],y_p)
    e = time.time()
    Processingtime[i]= e-s 
    #joblib.dump(weight,'weight.pickle')
  
print("Accuracy = %.4f" % np.mean(acc))
print("CPUtime = %.4f sec" % np.mean(Processingtime))
print("----------------------------------------------------")    



print("SVM with linear kernel")
acc = np.empty((n_folds))
Processingtime = np.empty((n_folds))
cv = KFold(n_splits=n_folds, shuffle=True, random_state=58310798)
for i, (train, test) in enumerate(cv.split(Xdata)):
    s = time.time()
    model = svm.SVC(kernel='linear')
#    model = svm.SVC(kernel='poly')    
#    model = svm.SVC(kernel='sigmoid')
#    model = svm.SVC(kernel='rbf')
    weight=model.fit(Xdata[train],ydata[train])
    #model = KNeighborsClassifier(n_neighbors = 7, n_jobs =2)
    #model.fit(Xdata[train],ydata[train])
    y_p = model.predict(Xdata[test]) 
    acc[i]= accuracy_score(ydata[test],y_p)
    e = time.time()
    Processingtime[i]= e-s 
    #joblib.dump(weight,'weight.pickle')

print("Accuracy = %.4f" % np.mean(acc))
print("CPUtime = %.4f sec" % np.mean(Processingtime))
print("----------------------------------------------------")   


#after processing Visualization can be use only centers = 2
xbg = xfix
ybg = yfix
xx1 = xbg[0]
yy1 = xbg[1]
xx2 = ybg[0]
yy2 = ybg[1]


xxm = (xx1+xx2)/2
yym = (yy1+yy2)/2
ms = (yy2-yy1)/(xx2-xx1)
mp = 1/ms
mp = -mp
xx = np.linspace(-5, 5)
yys = (ms*(xx-xx1)) + yy1  # เส้นเชื่อมจุด xbg,ybg
yyp = (mp*(xx-xxm)) + yym  # เส้นตั้งฉาก segment xbg,ybg

# Plot classifier and support vector 
w = model.coef_[0] # สัมประสิทธิ์ classifier
a = -w[0] / w[1] #  bias
yy = a * xx - (model.intercept_[0]) / w[1] # SVM classifier 
b = model.support_vectors_[0] # คำนวณหา suppor vectors ของ class 1
yy_down = a * xx + (b[1] - a * b[0]) # support vectors ของ class 1
b = model.support_vectors_[-1] # คำนวณหา support vectors ของ class 0 
yy_up = a * xx + (b[1] - a * b[0]) # support vectors ของ class 0 

#plot กราฟ 2 มิติ
ax = plt.subplot(1, 1, 1, aspect='equal') # ตีกรอบระนาบ xy
plt.scatter(Xdata[:,0],Xdata[:,1],marker='x',c=ydata) # ลงจุดข้อมูลทั้งหมด

plt.scatter(xx1, yy1, marker = 'o', c='blue') # จุด xbg
plt.scatter(xx2, yy2, marker = 'o', c='red') # จุด ybg
plt.plot(xx,yys,'k--',c='black') # plot เส้นตั้งฉาก classifier
plt.plot(xx,yyp,'k-',c='black') # plot classifier

plt.plot(xx, yy, 'k-', c='green') # plot SVM classifier 
plt.plot(xx, yy_down, 'k--', c='green') # plot suppor vectors ของ class 1
plt.plot(xx, yy_up, 'k--', c='green') # plot suppor vectors ของ class 0


z = lambda x,y: (-model.intercept_[0]-model.coef_[0][0]*x-model.coef_[0][1]) / model.coef_[0][2]


 # plot confidence ellipsoid
tmp = np.linspace(-5,1,4)
x,y = np.meshgrid(tmp,tmp)
EX,lambdaX_,vX = PSO.ConfidenceEllipsoid(X)
EY,lambdaY_,vY = PSO.ConfidenceEllipsoid(Y)
for j in range(1,2):
    ell1 = Ellipse(xy=(np.mean(X[:,0]), np.mean(X[:,1])),
              width=lambdaX_[0]*2*j, height=lambdaX_[1]*2*j,
              angle=np.rad2deg(np.arccos(vX[0, 0])), color='blue')

    ell2 = Ellipse(xy=(np.mean(Y[:,0]), np.mean(Y[:,1])),
              width=lambdaY_[0]*2*j, height=lambdaY_[1]*2*j,
              angle=np.rad2deg(np.arccos(vY[0, 0])), color='red')
    ell1.set_facecolor('none')
    ell2.set_facecolor('none')
    ax.add_artist(ell1)
    ax.add_artist(ell2)

plt.show() # plot กราฟ 